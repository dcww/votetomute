package com.stimicode.ostrichmann.votetomute.main;

import com.stimicode.ostrichmann.votetomute.Threading.Tasks.VoteCooldownTimer;
import com.stimicode.ostrichmann.votetomute.Threading.Tasks.VoteHandlerTimer;
import com.stimicode.ostrichmann.votetomute.manager.Manager;
import com.stimicode.ostrichmann.votetomute.manager.MessageManager;
import com.stimicode.ostrichmann.votetomute.objects.TempMuteData;
import com.stimicode.ostrichmann.votetomute.objects.VoteMuteData;
import com.stimicode.ostrichmann.votetomute.util.C;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

/**
 * Created by Derrick on 9/19/2015.
 */
public class Global {

    public static JavaPlugin plugin;

    public static HashMap<String, VoteMuteData> activeVotes = new HashMap<>();
    public static HashMap<String, TempMuteData> voteHistory = new HashMap<>();

    public static int votePercent;

    public static void init(JavaPlugin plug) {
        plugin = plug;
        try {
            votePercent = Manager.settingsManager.getInteger("vote-percent");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void handleVote(VoteMuteData voteMuteData) throws Exception {
        if (voteMuteData.mutePlayer()) {
            Bukkit.dispatchCommand(Bukkit.getServer().getConsoleSender(), "mute " + voteMuteData.getTarget().getName().toLowerCase() + " 1d");
            MessageManager.sendGlobal("Vote to mute " + voteMuteData.getTarget().getDisplayName() + " has ended. [" + voteMuteData.getYesCount() + "/" + voteMuteData.getServerCount() + "]");
            MessageManager.sendGlobal(voteMuteData.getTarget().getDisplayName() + " has been muted. (1 Day Mute)");
        } else {
            MessageManager.sendGlobal("Vote to mute " + voteMuteData.getTarget().getDisplayName() + " has ended. [" + voteMuteData.getYesCount() + "/" + voteMuteData.getServerCount() + "]");
            MessageManager.sendGlobal(voteMuteData.getTarget().getDisplayName() + " will not be muted.");
        }
        activeVotes.remove(voteMuteData.getTarget().getName().toLowerCase());
        TempMuteData temp = new TempMuteData(voteMuteData.getTarget().getName().toLowerCase(), new Date());
        voteHistory.put(voteMuteData.getTarget().getName().toLowerCase(), temp);
        VoteCooldownTimer.addQueue(temp);
    }

    public static boolean createVote(Player voteStarter, Player target) {
        if (!(voteHistory.containsKey(target.getName().toLowerCase()))) {
            VoteMuteData temp = new VoteMuteData(voteStarter, target);
            activeVotes.put(target.getName().toLowerCase(), temp);
            VoteHandlerTimer.addQueue(temp);
            return true;
        }
        return false;
    }

    public static boolean hasActiveVote(Player target) {
        return activeVotes.containsKey(target.getName().toLowerCase());
    }

    @Deprecated
    public static Player getPlayer(String _playerName) {
        return Bukkit.getPlayer(_playerName);
    }

    public static Player getPlayer(UUID _playerUUID) {
        return Bukkit.getPlayer(_playerUUID);
    }

    public static Player getPlayerUUID(String _playerUUID) {
        return Bukkit.getPlayer(UUID.fromString(_playerUUID));
    }
}
