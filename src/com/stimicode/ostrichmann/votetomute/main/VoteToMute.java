package com.stimicode.ostrichmann.votetomute.main;

import com.stimicode.ostrichmann.votetomute.Threading.TaskMaster;
import com.stimicode.ostrichmann.votetomute.Threading.Tasks.VoteCooldownTimer;
import com.stimicode.ostrichmann.votetomute.Threading.Tasks.VoteHandlerTimer;
import com.stimicode.ostrichmann.votetomute.commands.VoteMute;
import com.stimicode.ostrichmann.votetomute.manager.Manager;
import com.stimicode.ostrichmann.votetomute.util.BukkitUtility;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Derrick on 9/19/2015.
 */
public class VoteToMute extends JavaPlugin {

    public JavaPlugin instance;

    @Override
    public void onEnable() {
        initialize();
        TaskMaster.asyncTimer("VoteHandlerTimer", new VoteHandlerTimer(), 20);
        TaskMaster.asyncTimer("VoteCooldownTimer", new VoteCooldownTimer(), 20);
        this.saveDefaultConfig();
    }

    @Override
    public void onDisable() {
        //TODO On Disable Code
    }

    public void initialize() {
        setInstance(this);
        this.saveDefaultConfig();
        Log.init(getInstance());
        initCommands();
        initListeners();
        BukkitUtility.initialize(this);
        Manager.init(getInstance());
    }

    public void initCommands() {
        getCommand("votemute").setExecutor(new VoteMute());
    }

    public void initListeners() {

    }

    public JavaPlugin getInstance() {
        return instance;
    }

    public void setInstance(JavaPlugin instance) {
        this.instance = instance;
    }
}
