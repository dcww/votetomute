package com.stimicode.ostrichmann.votetomute.commands;

import com.stimicode.ostrichmann.votetomute.main.Global;
import com.stimicode.ostrichmann.votetomute.manager.MessageManager;
import com.stimicode.ostrichmann.votetomute.objects.VoteMuteData;
import com.stimicode.ostrichmann.votetomute.util.C;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Derrick on 10/10/2015.
 */
public class VoteMute implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {

        if (!(sender instanceof Player)) {
            MessageManager.sendError(sender, "Only a player can execute this command.");
            return false;
        }

        Player player = (Player) sender;

        if (args.length < 1) {
            MessageManager.sendError(player, "Enter a player name.");
            return false;
        }

        Player target = Global.getPlayer(args[0]);
        if (target == null) {
            MessageManager.sendError(player, "No players matching that name.");
            return false;
        }

        if (target.isOp() || target.hasPermission("civcraft.admin")) {
            MessageManager.sendError(player, "This player is exempt from being muted.");
            return false;
        }

        if (Global.hasActiveVote(target)) {
            VoteMuteData vote = Global.activeVotes.get(target.getName().toLowerCase());
            if (!(vote.canVote(player))) {
                MessageManager.sendError(player, "You cannot participate in this vote. (You logged in after the vote started)");
                return false;
            }

            if (vote.hasVoted(player)) {
                MessageManager.sendError(player, "You have already participated in this vote.");
                return false;
            }
            vote.addVote(player);
            MessageManager.send(player, "You have voted to mute " + vote.getTarget().getDisplayName() + ".");
        } else {
            if (Global.createVote(player, target)) {
                MessageManager.send(player, "You have started a vote to mute " + target.getDisplayName() + "!");
                Bukkit.broadcastMessage(C.Aqua + "[Global] " + C.White + "A vote has started to mute " + target.getDisplayName() + "! Type /votemute " + target.getName().toLowerCase() + " if you agree they should be muted.");
            } else {
                MessageManager.sendError(player, "Player has already been voted against. Please try again in a few minutes.");
                return false;
            }
        }
        return true;
    }
}
