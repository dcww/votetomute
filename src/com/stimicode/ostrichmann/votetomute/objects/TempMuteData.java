package com.stimicode.ostrichmann.votetomute.objects;

import java.util.Date;

/**
 * Created by Derrick on 10/10/2015.
 */
public class TempMuteData {

    public String target;
    public Date date;

    public TempMuteData(String target, Date date) {
        this.target = target;
        this.date = date;
    }
}
