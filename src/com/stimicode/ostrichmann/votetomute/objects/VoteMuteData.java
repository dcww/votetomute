package com.stimicode.ostrichmann.votetomute.objects;

import com.stimicode.ostrichmann.votetomute.main.Global;
import com.stimicode.ostrichmann.votetomute.manager.Manager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Derrick on 10/10/2015.
 */
public class VoteMuteData {

    private Date date;
    private String target;
    private String voteStarter;
    private int serverCount;
    private int yesCount;
    private ArrayList<String> onlinePlayers = new ArrayList<>();
    private ArrayList<String> voted = new ArrayList<>();

    public VoteMuteData(Player voteStarter, Player target) {
        this.target = target.getName().toLowerCase();
        this.voteStarter = voteStarter.getName().toLowerCase();
        this.date = new Date();
        this.serverCount = Bukkit.getOnlinePlayers().size();
        this.yesCount = 1;
        this.voted.add(voteStarter.getName().toLowerCase());
        onlinePlayers();
    }

    public void onlinePlayers() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            onlinePlayers.add(player.getName().toLowerCase());
        }
    }

    public Player getTarget() {
        return Global.getPlayer(this.target);
    }

    public Player getVoteStarter() {
        return Global.getPlayer(this.voteStarter);
    }

    public Date getDate() {
        return this.date;
    }

    public int getServerCount() {
        return this.serverCount;
    }

    public int getYesCount() {
        return this.yesCount;
    }

    public int getPercentYes() {
        return this.yesCount / this.serverCount;
    }

    public boolean mutePlayer() throws Exception {
        return (((double)this.yesCount / (double)this.serverCount) >= Manager.settingsManager.getDouble("vote-percent"));
    }

    public void addVote(Player player) {
        yesCount = (yesCount+1);
        voted.add(player.getName().toLowerCase());
    }

    public boolean hasVoted(Player player) {
        return voted.contains(player.getName().toLowerCase());
    }

    public boolean canVote(Player player) {
        return onlinePlayers.contains(player.getName().toLowerCase());
    }
}
