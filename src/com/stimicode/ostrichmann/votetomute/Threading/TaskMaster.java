package com.stimicode.ostrichmann.votetomute.Threading;


import com.stimicode.ostrichmann.votetomute.util.BukkitUtility;
import org.bukkit.scheduler.BukkitTask;

import java.util.HashMap;

/**
 * Created by Derrick on 6/21/2015.
 */
public class TaskMaster {

    private static HashMap<String, BukkitTask> tasks = new HashMap<>();
    private static HashMap<String, BukkitTask> timers = new HashMap<>();

    public static void syncTask(Runnable runnable) {
        BukkitUtility.scheduleSyncDelayedTask(runnable, 0);
    }

    public static void syncTask(Runnable runnable, long l) {
        BukkitUtility.scheduleSyncDelayedTask(runnable, l);
    }

    public static void asyncTimer(String name, Runnable runnable, long delay, long repeat) {
        addTimer(name, BukkitUtility.scheduleAsyncRepeatingTask(runnable, delay, repeat));
    }

    public static void asyncTimer(String name, Runnable runnable, long time) {
        addTimer(name, BukkitUtility.scheduleAsyncRepeatingTask(runnable, time, time));
    }

    public static void asyncTask(String name, Runnable runnable, long delay) {
        addTask(name, BukkitUtility.scheduleAsyncDelayedTask(runnable, delay));
    }

    public static void asyncTask(Runnable runnable, long delay) {
        BukkitUtility.scheduleAsyncDelayedTask(runnable, delay);
    }

    private static void addTimer(String name, BukkitTask timer) {
        timers.put(name, timer);
    }

    private static void addTask(String name, BukkitTask task) {
        tasks.put(name, task);
    }

    public static BukkitTask getTimer(String name) {
        return timers.get(name);
    }

    public static BukkitTask getTask(String name) {
        return tasks.get(name);
    }

    public static void syncTimer(String name, Runnable runnable, long time) {
        BukkitUtility.scheduleSyncRepeatingTask(runnable, time, time);
    }

    public static void syncTimer(String name, Runnable runnable, long delay, long repeat) {
        BukkitUtility.scheduleSyncRepeatingTask(runnable, delay, repeat);
    }
}
