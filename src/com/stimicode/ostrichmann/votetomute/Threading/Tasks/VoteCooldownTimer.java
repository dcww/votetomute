package com.stimicode.ostrichmann.votetomute.Threading.Tasks;

import com.stimicode.ostrichmann.votetomute.main.Global;
import com.stimicode.ostrichmann.votetomute.manager.Manager;
import com.stimicode.ostrichmann.votetomute.objects.TempMuteData;

import java.util.Date;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;

/**
 * Created by Derrick on 10/10/2015.
 */
public class VoteCooldownTimer implements Runnable {

    private static ConcurrentLinkedQueue<TempMuteData> queue = new ConcurrentLinkedQueue<>();

    @Override
    public void run() {

        for (int x = 0; x < 50; x++) {
            TempMuteData tempMuteData = queue.poll();
            if (tempMuteData == null) {
                return;
            }

            try {
                if (tempMuteData.date.getTime() + TimeUnit.SECONDS.toMillis(Manager.settingsManager.getInteger("vote-cooldown")) < new Date().getTime()) {
                    Global.voteHistory.remove(tempMuteData.target);
                    continue;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            queue.add(tempMuteData);
        }
    }

    public static void addQueue(TempMuteData tempMuteData) { queue.add(tempMuteData); }
}