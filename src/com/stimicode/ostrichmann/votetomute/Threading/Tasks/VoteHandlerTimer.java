package com.stimicode.ostrichmann.votetomute.Threading.Tasks;

import com.stimicode.ostrichmann.votetomute.main.Global;
import com.stimicode.ostrichmann.votetomute.manager.Manager;
import com.stimicode.ostrichmann.votetomute.objects.VoteMuteData;

import java.util.Date;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;

/**
 * Created by Derrick on 10/10/2015.
 */
public class VoteHandlerTimer implements Runnable {

    private static ConcurrentLinkedQueue<VoteMuteData> queue = new ConcurrentLinkedQueue<>();

    @Override
    public void run() {

        for (int x = 0; x < 50; x++) {
            VoteMuteData voteMuteData = queue.poll();
            if (voteMuteData == null) {
                return;
            }

            try {
                if (voteMuteData.getDate().getTime() + TimeUnit.SECONDS.toMillis(Manager.settingsManager.getInteger("vote-timeout")) < new Date().getTime()) {
                    Global.handleVote(voteMuteData);
                    continue;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            queue.add(voteMuteData);
        }
    }

    public static void addQueue(VoteMuteData voteMuteData) { queue.add(voteMuteData); }
}
